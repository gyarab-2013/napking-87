﻿using Gwen;
using Gwen.Control;

namespace Napking_87.Util
{
    internal static class MessageBox
    {
        public delegate void Button1Delegate();

        public delegate void Button2Delegate();

        public static Button ShowMessageBox(Base parent, string title, string text, string buttonText)
        {
            var messageBox = new WindowControl(parent, title, true);
            messageBox.MakeModal(true);
            messageBox.SetSize(300, 100);
            messageBox.Position(Pos.Center);
            messageBox.DeleteOnClose = true;
            messageBox.IsClosable = false;
            //messageBox.DisableResizing();

            var label = new Label(messageBox) {Text = text};
            label.SizeToContents();
            label.Dock = Pos.Top;

            var button = new Button(messageBox) {Text = buttonText};
            button.SizeToContents();
            button.SetSize(100, 30);
            button.Dock = Pos.Bottom;
            button.Clicked += delegate { messageBox.Close(); };

            if (label.Width > 300) messageBox.SetSize(label.Width + 20, 100);

            return button;
        }

        public static void ShowYesNoMessageBox(Base parent, string title, string text, string text2, string button1Text,
                                               string button2Text,
                                               Button1Delegate button1Delegate, Button2Delegate button2Delegate)
        {
            var messageBox = new WindowControl(parent, title, true);
            messageBox.MakeModal(true);
            messageBox.SetSize(200, 150);
            messageBox.Position(Pos.Center);
            messageBox.DeleteOnClose = true;
            messageBox.IsClosable = false;
            messageBox.DisableResizing();

            var label = new Label(messageBox) {Text = text};
            label.SizeToContents();
            label.Dock = Pos.Top;

            var label2 = new Label(messageBox) {Text = text2};
            label2.SizeToContents();
            label2.Dock = Pos.Top;

            var button2 = new Button(messageBox) {Text = button2Text};
            button2.SizeToContents();
            button2.SetSize(100, 30);
            button2.Dock = Pos.Bottom;
            if (button2Delegate != null)
            {
                button2.Clicked += delegate
                    {
                        button2Delegate.Invoke();
                        messageBox.Close();
                    };
            }
            else
            {
                button2.Clicked += delegate { messageBox.Close(); };
            }

            var button1 = new Button(messageBox) {Text = button1Text};
            button1.SizeToContents();
            button1.SetSize(100, 30);
            button1.Dock = Pos.Bottom;
            if (button1Delegate != null)
            {
                button1.Clicked += delegate
                    {
                        button1Delegate.Invoke();
                        messageBox.Close();
                    };
            }
            else
            {
                button1.Clicked += delegate { messageBox.Close(); };
            }

            messageBox.Width = label.Width + 15;
        }
    }
}