﻿using System;
using System.IO;
using System.Reflection;
using KuubEngine.Core;
using KuubEngine.State;
using Gwen.Control;
using Napking_87.GUIs;

namespace Napking_87.States
{
    public delegate void NapEvent(object sender, float abort);

    internal class MainState : GameState
    {
        public MainMenu MainMenu;
        public MoneyGui MoneyGui;
        public NapGui NapGui;
        public OptionsGui OptionsGui;
        public HelpGui HelpGui;
        public IntroGui IntroGui;
        public InventoryGui InventoryGui;
        public UpgradeGui UpgradeGui;
        public StoryGui StoryGui;
        public QuestSelectorGui QuestSelectorGui;

        public event NapEvent FinishedNap;
        public bool Nap;
        public TimeSpan CurrentNap;
        private TimeSpan _napLength;

        public TimeSpan NapLength
        {
            set
            {
                if (!Nap)
                {
                    _napLength = value;
                    CurrentNap = value;
                }
            }
            get { return _napLength; }
        }

        public MainState(KuubGameWindow game) : base(game)
        {
        }

        public override void Start()
        {
            MainMenu = new MainMenu(Game);
            MoneyGui = new MoneyGui(Game);
            NapGui = new NapGui(Game);
            OptionsGui = new OptionsGui(Game);
            HelpGui = new HelpGui(Game);
            IntroGui = new IntroGui(Game);
            UpgradeGui = new UpgradeGui(Game);
            StoryGui = new StoryGui(Game);
            QuestSelectorGui = new QuestSelectorGui(Game);

            InventoryGui = new InventoryGui(Game);
            InventoryGui.UpdateItems();

            Game.Closing += delegate { Exit(); };

            if (Player.First == 1)
            {
                MainMenu.ToggleWindow();
            }
            else
            {
                IntroGui.ToggleWindow();
            }
        }

        public override void Update(GameTime gameTime)
        {
            MainMenu.Update();
            MoneyGui.Update();
            NapGui.Update();
            OptionsGui.Update();
            HelpGui.Update();
            IntroGui.Update();
            InventoryGui.Update();
            UpgradeGui.Update();
            StoryGui.Update();
            QuestSelectorGui.Update();

            if (Nap)
            {
                CurrentNap -= gameTime.ElapsedGameTime;
                Player.TotalNapTime += gameTime.ElapsedGameTime;
                if (CurrentNap.TotalSeconds <= 0)
                {
                    FinishNap(true);
                }
                for (int i = 0; i < 3; i++)
                {
                    Item item = Player.Items[i];
                    if (item == null) continue;
                    if (item.TimeLeft == -1) continue;
                    item.TimeLeft -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (item.TimeLeft <= 0) Player.EquipmentChanged(item, false);
                    else
                    {
                        continue;
                    }
                    Util.MessageBox.ShowMessageBox(Game.GwenCanvas, ((Main)Game).ActiveLanguage.Get("General_alert"),
                                                   ((Main)Game).ActiveLanguage.Get("Item_your") + item.Name +
                                                   ((Main)Game).ActiveLanguage.Get("Item_broke"),
                                                   ((Main)Game).ActiveLanguage.Get("General_ok"));
                    DoNap();
                }
            }
        }

        public void LanguageChanger(object sender, ItemSelectedEventArgs args)
        {
            ((Main) Game).ActiveLanguage = ((Main) Game).LoadedLanguages[args.SelectedItem.Name];
            Player.SavedLanguage = args.SelectedItem.Name;

            UpdateStrings();

            MoneyGui.UpdateBankStatus();
            NapGui.UpdateNapButton();
            NapGui.SliderChange(this, EventArgs.Empty);
        }

        public void UpdateStrings()
        {
            foreach (Item item in ((Main) Game).GameItems)
            {
                if (item != null) item.ReloadName(((Main) Game).ActiveLanguage);
            }

            foreach (Quest quest in ((Main)Game).GameQuests)
            {
                if (quest != null) quest.ReloadInfo(((Main)Game).ActiveLanguage);
            }

            foreach (Item item in Player.Items)
            {
                if (item != null) item.ReloadName(((Main) Game).ActiveLanguage);
            }

            MainMenu.LoadLanguage();
            MoneyGui.LoadLanguage();
            NapGui.LoadLanguage();
            OptionsGui.LoadLanguage();
            HelpGui.LoadLanguage();
            IntroGui.LoadLanguage();
            InventoryGui.LoadLanguage();
            UpgradeGui.LoadLanguage();
            StoryGui.LoadLanguage();
            QuestSelectorGui.LoadLanguage();

        }

        public void Wipe()
        {
            string lang = Player.SavedLanguage;
            File.Delete(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/player.xml");
            Player.Load(Game);
            Player.SavedLanguage = lang;
            Player.Save();

            OptionsGui.Window.Close();
            MainMenu.Window.Close();
            HelpGui.Window.Close();
            NapGui.Window.Close();
            MoneyGui.Window.Close();
            InventoryGui.Window.Close();
            UpgradeGui.Window.Close();
            StoryGui.Window.Close();
            QuestSelectorGui.Window.Close();
            IntroGui.IntroState = 1;
            IntroGui.ToggleWindow();
            IntroGui.Intro(1);

            UpdateStrings();
            MoneyGui.UpdateBankStatus();
            NapGui.UpdateNapButton();
            NapGui.SliderChange(this, EventArgs.Empty);
            InventoryGui.UpdateItems();
            StoryGui.LoadQuest(Player.Quest, true);
        }

        public void Unload()
        {
            IntroGui.Unload();
        }

        public void Exit()
        {
            Player.Save();
            Game.Exit();
        }

        public float GetPercentDone()
        {
            return 1f - (float) (CurrentNap.TotalSeconds/_napLength.TotalSeconds);
        }

        public void FinishNap(bool succeess = false)
        {
            Nap = false;
            if (succeess) FinishedNap(this, -1f);
            else FinishedNap(this, GetPercentDone()/100*30);
            CurrentNap = _napLength;
            for (int i = 0; i < 3; i++)
            {
                Item item = Player.Items[i];
                if (item == null) continue;
                item.TimeLeft = (float) Math.Round(item.TimeLeft, 0);
                if (item.UsesLeft == -1) continue;
                item.UsesLeft--;
                if (item.UsesLeft == 0)
                {
                    Player.EquipmentChanged(item, false);
                    Util.MessageBox.ShowMessageBox(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("General_alert"),
                                                   ((Main) Game).ActiveLanguage.Get("Item_your") + item.Name +
                                                   ((Main) Game).ActiveLanguage.Get("Item_broke"),
                                                   ((Main) Game).ActiveLanguage.Get("General_ok"));
                }
            }

            InventoryGui.UpdateItems();
            Player.Save();
        }

        public void DoNap()
        {
            if (Nap)
            {
                FinishNap();
            }
            else
            {
                Nap = true;
            }
        }
    }
}