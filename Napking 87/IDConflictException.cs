﻿using System;

namespace Napking_87
{
    public class IDConflictException : Exception
    {
        public IDConflictException(string message) : base(message)
        {
        }
    }
}