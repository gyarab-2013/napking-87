﻿using System;

namespace Napking_87
{
    public class LuaHelper
    {
        public float Money
        {
            get { return Player.Money; }
            set { Player.Money = value; }
        }

        public int QuestVar
        {
            get { return Player.QuestVar; }
            set { Player.QuestVar = value; }
        }

        public int NapTime
        {
            get { return (int) Player.TotalNapTime.TotalSeconds; }
            set { Player.TotalNapTime += new TimeSpan(0, 0, value); }
        }

        public Item BedItem
        {
            get { return Player.Items[0]; }
        }

        public Item PillItem
        {
            get { return Player.Items[1]; }
        }

        public Item DrinkItem
        {
            get { return Player.Items[2]; }
        }
    }
}