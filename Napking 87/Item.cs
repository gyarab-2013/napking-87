﻿using System;
using System.Xml;
using System.IO;
using System.Reflection;

namespace Napking_87
{
    public class Item
    {
        private string _itemPath;
        public string Name;
        public int ID;
        public string Icon;
        public float NapLengthModifier;
        public float IncomeModifier;
        public float Price;
        public float TimeLeft;
        public int UsesLeft;
        public ItemType Type;
        public bool Buyable;

        public enum ItemType
        {
            Bed,
            Pill,
            Drink
        };

        public virtual bool Buy()
        {
            if (Player.Money >= Price)
            {
                Player.Money -= Price;
                Player.EquipmentChanged(Clone(), true);
                return true;
            }
            return false;
        }

        public Item Clone()
        {
            Item item = new Item();
            item._itemPath = _itemPath;
            item.Name = Name;
            item.ID = ID;
            item.Icon = Icon;
            item.NapLengthModifier = NapLengthModifier;
            item.IncomeModifier = IncomeModifier;
            item.Price = Price;
            item.TimeLeft = TimeLeft;
            item.UsesLeft = UsesLeft;
            item.Type = Type;
            item.Buyable = Buyable;

            return item;
        }

        public virtual void ReloadName(KuubEngine.Managers.Language activeLanguage)
        {
            var doc = new XmlDocument();

            if (File.Exists(_itemPath))
            {
                doc.Load(_itemPath);
            }
            else
            {
                throw new Exception("Item " + _itemPath + " not found!");
            }

            XmlNode root = doc.DocumentElement;

            foreach (XmlNode node in root)
            {
                if (node.Name == "name")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var element = (XmlElement) subNode;

                        if (element.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                        {
                            Name = element.InnerText;
                        }
                    }
                }
            }
        }

        public static Item LoadFromXml(String path, KuubEngine.Managers.Language activeLanguage)
        {
            Item item = new Item();

            item._itemPath = path;

            var doc = new XmlDocument();

            if (File.Exists(path))
            {
                doc.Load(path);
            }
            else
            {
                throw new FileLoadException("Item " + path + " not found!");
            }

            XmlNode root = doc.DocumentElement;

            foreach (XmlNode node in root.ChildNodes)
            {
                var element = (XmlElement)node;
                if (node.Name == "id")
                {
                    item.ID = int.Parse(element.InnerText);
                }
                else if (node.Name == "icon")
                {
                    if (
                        File.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                    "/Resources/Icons/" + element.InnerText))
                    {
                        item.Icon = "Resources/Icons/" + element.InnerText;
                    }
                    else
                    {
                        item.Icon = "Resources/Icons/missing_tex.png";
                    }
                }
                else if (node.Name == "type")
                {
                    string typ = node.InnerText;
                    if (typ == "Bed") item.Type = ItemType.Bed;
                    if (typ == "Pills") item.Type = ItemType.Pill;
                    if (typ == "Drink") item.Type = ItemType.Drink;
                }
                else if (node.Name == "name")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var subElement = (XmlElement) subNode;
                        if (subElement.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                            item.Name = subElement.InnerText;
                    }
                }
                else if (node.Name == "price")
                {
                    item.Price = float.Parse(element.InnerText);
                }
                else if (node.Name == "napLengthModifier")
                {
                    item.NapLengthModifier = float.Parse(element.InnerText);
                }
                else if (node.Name == "incomeModifier")
                {
                    item.IncomeModifier = float.Parse(element.InnerText);
                }
                else if (node.Name == "maxUses")
                {
                    item.UsesLeft = int.Parse(element.InnerText);
                }
                else if (node.Name == "maxTimeUses")
                {
                    item.TimeLeft = float.Parse(element.InnerText);
                }
                else if (node.Name == "buyable")
                {
                    item.Buyable = bool.Parse(element.InnerText);
                }
            }
            return item;
        }
    }
}