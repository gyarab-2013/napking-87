﻿using Gwen;
using KuubEngine.Core;
using Gwen.Control;

namespace Napking_87.GUIs
{
    internal class MoneyGui : GuiWindow
    {
        private Label _moneyLabel;
        private Label _moneyMinuteLabel;

        public MoneyGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Bank_title"));
            Window.SetPosition(200, 300);
            Window.SetSize(240, 125);
            Window.Close();
            Window.DisableResizing();

            _moneyMinuteLabel = new Label(Window) {Text = ((Main) Game).ActiveLanguage.Get("Bank_incomeStart")};
            _moneyMinuteLabel.SizeToContents();
            _moneyMinuteLabel.Dock = Pos.Top;

            _moneyLabel = new Label(Window) {Text = ((Main) Game).ActiveLanguage.Get("Bank_moneyStart")};
            _moneyLabel.SizeToContents();
            _moneyLabel.Dock = Pos.Top;

            UpdateBankStatus();
        }

        public void UpdateBankStatus()
        {
            _moneyLabel.Text = ((Main) Game).ActiveLanguage.Get("Bank_money") + Player.Money +
                               ((Main) Game).ActiveLanguage.Get("Bank_currency");
            _moneyLabel.SizeToContents();
            _moneyMinuteLabel.Text = ((Main) Game).ActiveLanguage.Get("Bank_income") + Player.MoneyPerMinute +
                                     ((Main) Game).ActiveLanguage.Get("Bank_currency");
            _moneyMinuteLabel.SizeToContents();
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("Bank_title");
            _moneyLabel.Text = ((Main) Game).ActiveLanguage.Get("Bank_moneyStart");
            _moneyMinuteLabel.Text = ((Main) Game).ActiveLanguage.Get("Bank_incomeStart");
        }
    }
}