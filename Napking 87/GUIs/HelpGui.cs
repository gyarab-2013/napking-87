﻿using Gwen;
using KuubEngine.Core;
using Gwen.Control;

namespace Napking_87.GUIs
{
    internal class HelpGui : GuiWindow
    {
        private TreeControl _helpTree;
        private MultilineTextBox _helpText;
        private TreeNode _generalNode;
        private TreeNode _napNode;
        private TreeNode _shopNode;
        private TreeNode _bankNode;

        public HelpGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Help_title"));
            Window.SetSize(445, 200);
            Window.SetPosition(700, 700);
            Window.DisableResizing();
            Window.Close();

            _helpTree = new TreeControl(Window) {Dock = Pos.Left, Width = 230};
            _generalNode = _helpTree.AddNode(((Main) Game).ActiveLanguage.Get("General_general"));
            _napNode = _generalNode.AddNode(((Main) Game).ActiveLanguage.Get("NapLair_title"));
            _bankNode = _generalNode.AddNode(((Main) Game).ActiveLanguage.Get("Bank_title"));
            _shopNode = _generalNode.AddNode(((Main) Game).ActiveLanguage.Get("Shop_title"));

            // 25 chars per line

            _napNode.Clicked += delegate { _helpText.Text = ((Main) Game).ActiveLanguage.Get("Help_nap"); };
            _bankNode.Clicked += delegate { _helpText.Text = ((Main) Game).ActiveLanguage.Get("Help_bank"); };
            _shopNode.Clicked += delegate { _helpText.Text = ((Main) Game).ActiveLanguage.Get("Help_shop"); };

            _helpText = new MultilineTextBox(Window) {Dock = Pos.Right, Width = 185};
            _helpText.Clicked += delegate { Window.Focus(); };
            _helpText.RightClicked += delegate { Window.Focus(); };
        }

        public override void Update()
        {
            if (_helpText.HasFocus) Window.Focus();
        }

        public override void LoadLanguage()
        {
            _helpText.Text = ((Main) Game).ActiveLanguage.Get("Help_reload");
            _generalNode.Text = ((Main) Game).ActiveLanguage.Get("General_general");
            _napNode.Text = ((Main) Game).ActiveLanguage.Get("NapLair_title");
            _bankNode.Text = ((Main) Game).ActiveLanguage.Get("Bank_title");
            _shopNode.Text = ((Main) Game).ActiveLanguage.Get("Shop_title");
        }
    }
}