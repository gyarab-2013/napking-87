﻿using System;
using System.Timers;
using Gwen;
using KuubEngine.Core;
using Gwen.Control;

namespace Napking_87.GUIs
{
    internal class StoryGui : GuiWindow
    {
        private Button _nextButton;
        private Timer _questTimer;
        private QuestBox _questBox;

        public StoryGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Story_title"));
            Window.SetPosition(500, 300);
            Window.SetSize(430, 225);
            Window.Close();
            Window.DisableResizing();

            _nextButton = new Button(Window)
                {
                    Dock = Pos.Bottom,
                    Text = ((Main) Game).ActiveLanguage.Get("Story_finish")
                };
            _nextButton.Clicked += NextQuest;

            _questBox = new QuestBox(Game,null,Window) {GroupBox = {Dock = Pos.Top}};

            _questTimer = new Timer();
            _questTimer.Elapsed += QuestTick;
            _questTimer.Interval = 2000;
            _questTimer.Start();
        }

        private void NextQuest(Base sender, ClickedEventArgs arguments)
        {
            if (_nextButton.IsDisabled || ((States.MainState)((Main)Game).MainStateInstance).QuestSelectorGui.Window.IsVisible) return;
            ((States.MainState)((Main)Game).MainStateInstance).QuestSelectorGui.LoadNewQuests();
            ((States.MainState)((Main)Game).MainStateInstance).QuestSelectorGui.ToggleWindow();
        }

        private void QuestTick(object sender, EventArgs args)
        {
            bool finished = Player.Quest.CheckCondition(Game);

            if (finished)
            {
                _nextButton.Enable();
            }
            else
            {
                _nextButton.Disable();
            }
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("Story_title");
            _nextButton.Text = ((Main) Game).ActiveLanguage.Get("Story_finish");
            _questBox.LoadLanguage();
        }

        public void LoadQuest(Quest quest, bool save)
        {
            Player.Quest = quest;
            _questBox.LoadQuest(quest, save, true);
            QuestTick(this, EventArgs.Empty);

            Player.Save();
        }
    }
}