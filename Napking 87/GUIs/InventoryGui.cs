﻿using Gwen.Control;
using KuubEngine.Core;

namespace Napking_87.GUIs
{
    internal class InventoryGui : GuiWindow
    {
        public ItemBox BedItem;
        public ItemBox PillItem;
        public ItemBox DrinkItem;

        public InventoryGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Inventory_title"));
            Window.SetSize(420, 295);
            Window.SetPosition(300, 300);
            Window.DisableResizing();
            Window.Close();

            BedItem = new ItemBox(Game, Player.Items[0], Window);

            PillItem = new ItemBox(Game, Player.Items[1], Window);
            PillItem.GroupBox.SetPosition(0, 85);

            DrinkItem = new ItemBox(Game, Player.Items[2], Window);
            DrinkItem.GroupBox.SetPosition(0, 170);
        }

        public void UpdateItems()
        {
            BedItem.LoadItem(Player.Items[0]);
            PillItem.LoadItem(Player.Items[1]);
            DrinkItem.LoadItem(Player.Items[2]);
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("Inventory_title");
            BedItem.LoadLanguage();
            PillItem.LoadLanguage();
            DrinkItem.LoadLanguage();
        }
    }
}