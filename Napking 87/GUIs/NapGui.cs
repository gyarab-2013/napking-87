﻿using System;
using Gwen;
using KuubEngine.Core;
using Gwen.Control;

namespace Napking_87.GUIs
{
    internal class NapGui : GuiWindow
    {
        private Button _doNapButton;
        private ProgressBar _napProgress;
        private Slider _napSlider;
        private Label _napLabel;

        public NapGui(KuubGameWindow game)
        {
            Game = game;

            ((States.MainState)((Main) Game).MainStateInstance).NapLength = new TimeSpan(0, 0, 1, 0);
            ((States.MainState)((Main) Game).MainStateInstance).FinishedNap += NapHandler;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("NapLair_title"));
            Window.SetPosition(100, 100);
            Window.SetSize(200, 150);
            Window.Close();
            Window.DisableResizing();

            _doNapButton = new Button(Window);
            _doNapButton.Clicked += delegate { DoNap(); };
            _doNapButton.Dock = Pos.Top;

            _napProgress = new ProgressBar(Window) {Dock = Pos.Bottom, Value = 0f};

            _napSlider = new HorizontalSlider(Window) {Y = 60, Dock = Pos.CenterV, Height = 20};
            _napSlider.Width = Window.Width - 12;
            _napSlider.ValueChanged += SliderChange;

            _napLabel = new Label(Window)
                {
                    X = 22,
                    Y = 35,
                    Text = ((Main) Game).ActiveLanguage.Get("NapLair_napLength") + "00:00:00"
                };

            SliderChange(this, EventArgs.Empty);
            UpdateNapButton();
        }

        public override void Update()
        {
            _napProgress.Value = ((States.MainState)((Main)Game).MainStateInstance).GetPercentDone();
        }

        public void DoNap()
        {
            if (!(((States.MainState)((Main) Game).MainStateInstance).Nap))
            {
                ((States.MainState)((Main)Game).MainStateInstance).DoNap();
            }
            else
            {
                Util.MessageBox.ShowYesNoMessageBox(Game.GwenCanvas,
                                                    ((Main) Game).ActiveLanguage.Get("NapLair_cancelTitle"),
                                                    ((Main) Game).ActiveLanguage.Get("NapLair_cancelText1"),
                                                    ((Main) Game).ActiveLanguage.Get("NapLair_cancelText2"), "Yes", "No",
                                                    ((States.MainState)((Main) Game).MainStateInstance).DoNap, null);
            }

            UpdateNapButton();
        }

        public void SliderChange(object sender, EventArgs args)
        {
            ((States.MainState)((Main)Game).MainStateInstance).NapLength =
                new TimeSpan(0, 0, 0, (int) ((Player.MaxNapTime - Player.MinNapTime).TotalSeconds*_napSlider.Value)) +
                Player.MinNapTime;
            _napLabel.Text = ((Main) Game).ActiveLanguage.Get("NapLair_napLength") +
                             ((States.MainState)((Main)Game).MainStateInstance).NapLength;
            _napLabel.SizeToContents();
        }

        public void UpdateNapButton()
        {
            if (((States.MainState)((Main)Game).MainStateInstance).Nap)
            {
                _doNapButton.Text = ((Main) Game).ActiveLanguage.Get("NapLair_buttonState2");
                _doNapButton.SizeToContents();
            }
            else
            {
                _doNapButton.Text = ((Main) Game).ActiveLanguage.Get("NapLair_buttonState1");
                _doNapButton.SizeToContents();
            }
        }

        public void NapHandler(object sender, float abort)
        {
            SliderChange(this, EventArgs.Empty);
            UpdateNapButton();
            if (abort >= 0)
            {
                Util.MessageBox.ShowMessageBox(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("General_alert"),
                                               ((Main) Game).ActiveLanguage.Get("Nap_abort"),
                                               ((Main) Game).ActiveLanguage.Get("Nap_yawn"));
                Player.Money += (Player.MoneyPerMinute * (float)((States.MainState)((Main)Game).MainStateInstance).NapLength.TotalMinutes) *
                                abort;
                ((States.MainState) ((Main) Game).MainStateInstance).MoneyGui.UpdateBankStatus();
                return;
            }
            Util.MessageBox.ShowMessageBox(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Nap_success"),
                                           ((Main) Game).ActiveLanguage.Get("Nap_finish"),
                                           ((Main) Game).ActiveLanguage.Get("Nap_yawn"));
            Player.Money += Player.MoneyPerMinute * (float)((States.MainState)((Main)Game).MainStateInstance).NapLength.TotalMinutes;
            ((States.MainState) ((Main) Game).MainStateInstance).MoneyGui.UpdateBankStatus();
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("NapLair_title");
            _doNapButton.Text = ((Main) Game).ActiveLanguage.Get("NapLair_buttonState1");
            _napLabel.Text = ((Main) Game).ActiveLanguage.Get("NapLair_napLength") + "00:00:00";
        }
    }
}