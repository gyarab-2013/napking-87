﻿using System;
using Gwen;
using Gwen.Control;
using KuubEngine.Core;

namespace Napking_87.GUIs
{
    class QuestBox : GuiWindow
    {
        public GroupBox GroupBox;
        public Quest Quest;

        private Label _questName;
        private Label _questGoal;
        private Label _questDescription;
        private Label _rewardLabel;
        private ItemBox _reward;

        public QuestBox(KuubGameWindow game, Quest quest, Base parent)
        {
            Game = game;
            Quest = quest;

            GroupBox = new GroupBox(parent) { Width = 410, Height = 170 };

            _reward = new ItemBox(Game, null, GroupBox);
            _reward.GroupBox.Dock = Pos.Bottom;

            _rewardLabel = new Label(GroupBox)
            {
                Dock = Pos.Bottom,
                Text =
                    ((Main)Game).ActiveLanguage.Get("Story_reward") + "0" +
                    ((Main)Game).ActiveLanguage.Get("Bank_currency") + " + " +
                    ((Main)Game).ActiveLanguage.Get("Story_item")
            };

            _questName = new Label(GroupBox)
            {
                Dock = Pos.Top,
                Text = ((Main)Game).ActiveLanguage.Get("Story_name")
            };

            _questDescription = new Label(GroupBox)
            {
                Dock = Pos.Top,
                Text = ((Main)Game).ActiveLanguage.Get("Story_description")
            };

            _questGoal = new Label(GroupBox)
            {
                Dock = Pos.Top,
                Text = ((Main)Game).ActiveLanguage.Get("Story_goal")
            };
        }

        public void LoadQuest(Quest quest, bool save, bool activate)
        {
            Quest = quest;
            _reward.LoadItem(null);

            if (Quest == null)
            {
                LoadLanguage();
                return;
            }

            if (Quest.ItemIDReward != -1)
            {
                _reward.LoadItem(((Main)Game).GameItems[Quest.ItemIDReward]);
            }
            if (activate) Quest.LoadScript(Game);
            if (!save) Game.Lua.DoString("quest.init()");
            LoadLanguage();
        }

        public override void LoadLanguage()
        {
            if (Quest == null)
            {
                _rewardLabel.Text = ((Main)Game).ActiveLanguage.Get("Story_reward") + "0" +
                                    ((Main)Game).ActiveLanguage.Get("Bank_currency") + " + " +
                                    ((Main)Game).ActiveLanguage.Get("Story_item");
                _questName.Text = ((Main) Game).ActiveLanguage.Get("Story_name");
                _questDescription.Text = ((Main) Game).ActiveLanguage.Get("Story_description");
                _questGoal.Text = ((Main) Game).ActiveLanguage.Get("Story_goal");
            }
            else
            {
                Quest.ReloadInfo(((Main)Game).ActiveLanguage);
                _rewardLabel.Text = ((Main)Game).ActiveLanguage.Get("Story_reward") + Quest.MoneyReward +
                                    ((Main)Game).ActiveLanguage.Get("Bank_currency") + " + " +
                                    ((Main)Game).ActiveLanguage.Get("Story_item");
                _questName.Text = ((Main)Game).ActiveLanguage.Get("Story_name") + Quest.Name;
                _questDescription.Text = ((Main)Game).ActiveLanguage.Get("Story_description") + Quest.Description;
                _questGoal.Text = ((Main)Game).ActiveLanguage.Get("Story_goal") + Quest.Goal;
            }

            _reward.LoadLanguage();
        }
    }
}
