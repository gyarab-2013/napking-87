﻿using System;
using Gwen.Control;
using KuubEngine.Core;

namespace Napking_87.GUIs
{
    internal class ItemBox : GuiWindow
    {
        public GroupBox GroupBox;

        private ImagePanel _itemImage;
        private Label _price;
        private Label _useLeft;
        private Label _timeLeft;
        private Label _napLengthMultilplier;
        private Label _napIncomeMultiplier;
        private Label _itemName;

        private Item _item;

        public ItemBox(KuubGameWindow game, Item item, Base parent, bool shop = false)
        {
            Game = game;
            _item = item;
            GroupBox = new GroupBox(parent) {Width = 400, Height = 85};

            if (item == null)
            {
                _itemImage = new ImagePanel(GroupBox)
                    {
                        ImageName = "Resources/Icons/spr_empty.png",
                        Width = 64,
                        Height = 64,
                        Y = 0
                    };
                _itemName = new Label(GroupBox)
                    {
                        Text = ((Main) Game).ActiveLanguage.Get("General_empty"),
                        X = 75,
                        Y = -3
                    };
                return;
            }

            _itemImage = new ImagePanel(GroupBox) {ImageName = _item.Icon, Width = 64, Height = 64, Y = 0};
            _itemName = new Label(GroupBox) {Text = _item.Name, X = 75, Y = -3};
            _price = new Label(GroupBox)
                {
                    Text =
                        ((Main) Game).ActiveLanguage.Get("Shop_price") + _item.Price +
                        ((Main) Game).ActiveLanguage.Get("Bank_currency"),
                    X = 75,
                    Y = 12
                };

            if (_item.UsesLeft == -1)
            {
                _useLeft = new Label(GroupBox)
                    {
                        Text = ((Main) Game).ActiveLanguage.Get("Shop_usesLeft") + "∞",
                        X = 75,
                        Y = 30
                    };
            }
            else
            {
                _useLeft = new Label(GroupBox)
                    {
                        Text = ((Main) Game).ActiveLanguage.Get("Shop_usesLeft") + _item.UsesLeft,
                        X = 75,
                        Y = 30
                    };
            }

            if (_item.TimeLeft == -1)
            {
                _timeLeft = new Label(GroupBox)
                    {
                        Text = ((Main) Game).ActiveLanguage.Get("Shop_timeLeft") + "∞",
                        X = 75,
                        Y = 50
                    };
            }
            else
            {
                _timeLeft = new Label(GroupBox)
                    {
                        Text = ((Main) Game).ActiveLanguage.Get("Shop_timeLeft") + _item.TimeLeft + "s",
                        X = 75,
                        Y = 50
                    };
            }

            _napIncomeMultiplier = new Label(GroupBox)
                {
                    Text = ((Main) Game).ActiveLanguage.Get("Shop_napIncomeModifier") + _item.IncomeModifier,
                    X = 175,
                    Y = 6
                };
            _napLengthMultilplier = new Label(GroupBox)
                {
                    Text = ((Main) Game).ActiveLanguage.Get("Shop_napLengthModifier") + _item.NapLengthModifier,
                    X = 175,
                    Y = 35
                };

            if (!shop) return;

            _itemImage.Clicked += delegate
                {
                    Util.MessageBox.ShowYesNoMessageBox(Game.GwenCanvas,
                                                        ((Main) Game).ActiveLanguage.Get("Shop_confirm"),
                                                        ((Main) Game).ActiveLanguage.Get("Shop_purchase") + _item.Name +
                                                        ((Main) Game).ActiveLanguage.Get("Shop_for") + _item.Price +
                                                        ((Main) Game).ActiveLanguage.Get("Bank_currency") + "?", "",
                                                        ((Main) Game).ActiveLanguage.Get("General_yes"),
                                                        ((Main) Game).ActiveLanguage.Get("General_no"), Buy, null);
                };
        }

        public void LoadItem(Item item)
        {
            _item = item;

            if (item == null)
            {
                _itemImage.ImageName = "Resources/Icons/spr_empty.png";
                _itemName.Text = ((Main) Game).ActiveLanguage.Get("General_empty");

                if (_price != null)
                {
                    _price.Text = "";
                    _useLeft.Text = "";
                    _timeLeft.Text = "";
                    _napIncomeMultiplier.Text = "";
                    _napLengthMultilplier.Text = "";
                }
                return;
            }
            _itemImage.ImageName = _item.Icon;

            if (_price == null)
            {
                _price = new Label(GroupBox)
                    {
                        Text =
                            ((Main) Game).ActiveLanguage.Get("Shop_price") + _item.Price +
                            ((Main) Game).ActiveLanguage.Get("Bank_currency"),
                        X = 75,
                        Y = 12
                    };

                if (_item.UsesLeft == -1)
                {
                    _useLeft = new Label(GroupBox)
                        {
                            Text = ((Main) Game).ActiveLanguage.Get("Shop_usesLeft") + "∞",
                            X = 75,
                            Y = 30
                        };
                }
                else
                {
                    _useLeft = new Label(GroupBox)
                        {
                            Text = ((Main) Game).ActiveLanguage.Get("Shop_usesLeft") + _item.UsesLeft,
                            X = 75,
                            Y = 30
                        };
                }

                if (_item.TimeLeft == -1)
                {
                    _timeLeft = new Label(GroupBox)
                        {
                            Text = ((Main) Game).ActiveLanguage.Get("Shop_timeLeft") + "∞",
                            X = 75,
                            Y = 50
                        };
                }
                else
                {
                    _timeLeft = new Label(GroupBox)
                        {
                            Text = ((Main) Game).ActiveLanguage.Get("Shop_timeLeft") + Math.Round(_item.TimeLeft, 0) + "s",
                            X = 75,
                            Y = 50
                        };
                }

                _napIncomeMultiplier = new Label(GroupBox)
                    {
                        Text = ((Main) Game).ActiveLanguage.Get("Shop_napIncomeModifier") + _item.IncomeModifier,
                        X = 175,
                        Y = 6
                    };
                _napLengthMultilplier = new Label(GroupBox)
                    {
                        Text = ((Main) Game).ActiveLanguage.Get("Shop_napLengthModifier") + _item.NapLengthModifier,
                        X = 175,
                        Y = 35
                    };
            }

            LoadLanguage();
        }

        private void Buy()
        {
            bool success = _item.Buy();
            if (success)
                Util.MessageBox.ShowMessageBox(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Shop_success"),
                                               ((Main) Game).ActiveLanguage.Get("Shop_successFull") + _item.Name,
                                               ((Main) Game).ActiveLanguage.Get("General_ok"));
            else
                Util.MessageBox.ShowMessageBox(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Shop_unsuccess"),
                                               ((Main) Game).ActiveLanguage.Get("Shop_unsuccessFull"),
                                               ((Main) Game).ActiveLanguage.Get("General_ok"));
        }

        public override void LoadLanguage()
        {
            if (_item == null)
            {
                _itemName.Text = ((Main) Game).ActiveLanguage.Get("General_empty");
                return;
            }

            _price.Text = ((Main) Game).ActiveLanguage.Get("Shop_price") + _item.Price +
                          ((Main) Game).ActiveLanguage.Get("Bank_currency");
            _itemName.Text = _item.Name;

            if (_item.UsesLeft == -1)
            {
                _useLeft.Text = ((Main) Game).ActiveLanguage.Get("Shop_usesLeft") + "∞";
            }
            else
            {
                _useLeft.Text = ((Main) Game).ActiveLanguage.Get("Shop_usesLeft") + _item.UsesLeft;
            }

            if (_item.TimeLeft == -1)
            {
                _timeLeft.Text = ((Main) Game).ActiveLanguage.Get("Shop_timeLeft") + "∞";
            }
            else
            {
                _timeLeft.Text = ((Main) Game).ActiveLanguage.Get("Shop_timeLeft") + Math.Round(_item.TimeLeft, 0) + "s";
            }

            _napIncomeMultiplier.Text = ((Main) Game).ActiveLanguage.Get("Shop_napIncomeModifier") +
                                        _item.IncomeModifier;
            _napLengthMultilplier.Text = ((Main) Game).ActiveLanguage.Get("Shop_napLengthModifier") +
                                         _item.NapLengthModifier;
        }
    }
}