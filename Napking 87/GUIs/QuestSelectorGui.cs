﻿using Gwen;
using KuubEngine.Core;
using Gwen.Control;

namespace Napking_87.GUIs
{
    class QuestSelectorGui : GuiWindow
    {
        private ComboBox _questCombo;
        private Button _acceptQuestButton;
        private QuestBox _questBox;

        public QuestSelectorGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Quest_nextTitle"));
            Window.SetSize(430,250);
            Window.Position(Pos.Center);
            Window.DisableResizing();
            Window.Close();

            _acceptQuestButton = new Button(Window) {Dock = Pos.Bottom, Text = ((Main)Game).ActiveLanguage.Get("General_ok")};
            _acceptQuestButton.Clicked += NextQuest;

            _questCombo = new ComboBox(Window) {Width = 200, Dock = Pos.Top};
            _questCombo.ItemSelected += QuestSelected;

            _questBox = new QuestBox(Game, null, Window) {GroupBox = {Dock = Pos.Top}};
        }

        private void NextQuest(Base sender, ClickedEventArgs arguments)
        {
            if (_questCombo.Text == "") return;
            Player.Quest.Finish(Game);
            ((States.MainState)((Main)Game).MainStateInstance).StoryGui.LoadQuest(((Main)Game).GameQuests[int.Parse(_questCombo.SelectedItem.Name)], false);
            ToggleWindow();
        }

        private void QuestSelected(Base sender, ItemSelectedEventArgs arguments)
        {
            if (arguments.SelectedItem == null) return;
            _questBox.LoadQuest(((Main)Game).GameQuests[int.Parse(arguments.SelectedItem.Name)],true, false);
        }

        public void LoadNewQuests()
        {
            _questCombo.Clear();

            foreach (var quest in Player.Quest.NextQuests)
            {
                _questCombo.AddItem(((Main) Game).GameQuests[quest].Name, quest.ToString());
            }

            _questCombo.Text = "";
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("Quest_nextTitle");
            _acceptQuestButton.Text = ((Main) Game).ActiveLanguage.Get("General_ok");
            _questBox.LoadLanguage();
            LoadNewQuests();
        }

        public override void ToggleWindow()
        {
            _questBox.LoadQuest(null, true, false);
            base.ToggleWindow();
        }
    }
}
