﻿using Gwen;
using KuubEngine.Core;
using Gwen.Control;


namespace Napking_87.GUIs
{
    internal class OptionsGui : GuiWindow
    {
        private Button _saveButton;
        private Button _wipeButton;
        private ComboBox _languageCombo;

        public OptionsGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Options_title"));
            Window.SetPosition(800, 400);
            Window.SetSize(150, 125);
            Window.Close();
            Window.DisableResizing();

            _saveButton = new Button(Window)
                {
                    Dock = Pos.Top,
                    Text = ((Main) Game).ActiveLanguage.Get("Options_forceSave")
                };
            _saveButton.Clicked += delegate
                {
                    Util.MessageBox.ShowMessageBox(Game.GwenCanvas,
                                                   ((Main) Game).ActiveLanguage.Get("Options_saveCompleted"),
                                                   ((Main) Game).ActiveLanguage.Get("Options_saveText"),
                                                   ((Main) Game).ActiveLanguage.Get("General_ok"));
                    Player.Save();
                };

            _wipeButton = new Button(Window)
                {
                    Dock = Pos.Bottom,
                    Text = ((Main) Game).ActiveLanguage.Get("Options_wipeButton")
                };
            _wipeButton.Clicked += delegate
                {
                    Util.MessageBox.ShowYesNoMessageBox(Game.GwenCanvas,
                                                        ((Main) Game).ActiveLanguage.Get("Options_wipeTitle"),
                                                        ((Main) Game).ActiveLanguage.Get("Options_wipeText1"),
                                                        ((Main) Game).ActiveLanguage.Get("Options_wipeText2"),
                                                        ((Main) Game).ActiveLanguage.Get("Options_wipeButton1"),
                                                        ((Main) Game).ActiveLanguage.Get("Options_wipeButton2"),
                                                        ((States.MainState) ((Main) Game).MainStateInstance).Wipe, null);
                };

            _languageCombo = new ComboBox(Window) {Dock = Pos.Top, Margin = new Margin(0, 5, 0, 0)};
            foreach (var language in ((Main) Game).LoadedLanguages)
            {
                if (language.Value == ((Main) Game).ActiveLanguage)
                {
                    _languageCombo.AddItem(language.Value.Name, language.Key);
                }
            }
            foreach (var language in ((Main) Game).LoadedLanguages)
            {
                if (language.Value != ((Main) Game).ActiveLanguage)
                {
                    _languageCombo.AddItem(language.Value.Name, language.Key);
                }
            }
            _languageCombo.ItemSelected += ((States.MainState) ((Main) Game).MainStateInstance).LanguageChanger;
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("Options_title");
            _saveButton.Text = ((Main) Game).ActiveLanguage.Get("Options_forceSave");
            _wipeButton.Text = ((Main) Game).ActiveLanguage.Get("Options_wipeButton");
        }
    }
}