﻿using KuubEngine.Content;
using KuubEngine.Core;
using Gwen.Control;

namespace Napking_87.GUIs
{
    internal class IntroGui : GuiWindow
    {
        private ImagePanel _introImage;
        private Label _introLabel;
        private Button _introNext;
        private Button _introBack;
        public int IntroState = 1;
        private SoundEffect _introMusic;

        public IntroGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, "NapKing 87 Intro");
            Window.SetSize(900, 600);
            Window.IsClosable = false;
            Window.DisableResizing();

            _introImage = new ImagePanel(Window) {ImageName = "Resources/intro_1.png", Width = 888, Height = 500};

            _introLabel = new Label(Window) {Text = ((Main) Game).ActiveLanguage.Get("Genral_error")};
            _introLabel.SetPosition(0, 510);

            _introNext = new Button(Window) {Text = ((Main) Game).ActiveLanguage.Get("General_next")};
            _introNext.SetPosition(780, 540);
            _introNext.Clicked += delegate
                {
                    if (_introNext.IsDisabled) return;
                    Intro(++IntroState);
                };

            _introBack = new Button(Window) {Text = ((Main) Game).ActiveLanguage.Get("General_back")};
            _introBack.SetPosition(0, 540);
            _introBack.Clicked += delegate
                {
                    if (_introBack.IsDisabled) return;
                    Intro(--IntroState);
                };

            _introMusic = Content.Load<SoundEffect>("Resources/Music/intro_1.wav");
            _introMusic.Gain = 0.2f;

            Intro(IntroState);
        }

        public void Intro(int state)
        {
            switch (state)
            {
                case 1:
                    _introBack.IsDisabled = true;
                    _introNext.Text = ((Main) Game).ActiveLanguage.Get("General_next");
                    _introBack.Text = ((Main) Game).ActiveLanguage.Get("General_back");
                    _introLabel.Text = ((Main) Game).ActiveLanguage.Get("Intro_text1");
                    _introImage.ImageName = "Resources/intro_1.png";
                    _introMusic.Loop = true;
                    if (Player.First == 1 & !_introMusic.Playing) _introMusic.Play();
                    break;
                case 2:
                    _introBack.IsDisabled = false;
                    _introLabel.Text = ((Main) Game).ActiveLanguage.Get("Intro_text2");
                    _introImage.ImageName = "Resources/intro_2.png";
                    break;
                case 3:
                    _introLabel.Text = ((Main) Game).ActiveLanguage.Get("Intro_text3");
                    _introImage.ImageName = "Resources/intro_3.png";
                    break;
                case 4:
                    _introLabel.Text = ((Main) Game).ActiveLanguage.Get("Intro_text4");
                    _introNext.Text = ((Main) Game).ActiveLanguage.Get("General_next");
                    break;
                case 5:
                    _introNext.Text = ((Main) Game).ActiveLanguage.Get("General_done");
                    _introImage.ImageName = "Resources/intro_5.png";
                    _introLabel.Text = ((Main) Game).ActiveLanguage.Get("Intro_text5");
                    break;
                case 6:
                    ToggleWindow();
                    ((Main) Game).MainStateInstance.MainMenu.ToggleWindow();
                    Player.First = 0;
                    _introMusic.Stop();
                    break;
            }
        }

        public void Unload()
        {
            _introMusic.Unload();
        }

        public override void LoadLanguage()
        {
        }
    }
}