﻿using System.Collections.Generic;
using Gwen;
using Gwen.Control;
using KuubEngine.Core;

namespace Napking_87.GUIs
{
    internal class UpgradeGui : GuiWindow
    {
        private DockedTabControl _upgradeTabs;
        private TabButton _upgradeBed;
        private TabButton _upgradeEnergy;
        private TabButton _upgradePill;

        private ScrollControl _upgradeBedScroll;
        private ScrollControl _upgradePillScroll;
        private ScrollControl _upgradeDrinkScroll;

        private List<Item> _bedList;
        private List<Item> _pillList;
        private List<Item> _drinkList;

        public UpgradeGui(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("Shop_title"));
            Window.SetPosition(800, 300);
            Window.SetSize(450, 300);
            Window.Close();
            Window.DisableResizing();

            _upgradeTabs = new DockedTabControl(Window);
            _upgradePill = _upgradeTabs.AddPage(((Main) Game).ActiveLanguage.Get("Shop_pillTab"));
            _upgradeEnergy = _upgradeTabs.AddPage(((Main) Game).ActiveLanguage.Get("Shop_energyTab"));
            _upgradeBed = _upgradeTabs.AddPage(((Main) Game).ActiveLanguage.Get("Shop_bedTab"));

            _bedList = new List<Item>();
            _pillList = new List<Item>();
            _drinkList = new List<Item>();

            PopulateTabs();
        }

        public void PopulateTabs()
        {
            _bedList.Clear();
            _pillList.Clear();
            _drinkList.Clear();

            _upgradeBed.Page.DeleteAllChildren();
            _upgradeEnergy.Page.DeleteAllChildren();
            _upgradePill.Page.DeleteAllChildren();

            _upgradeBedScroll = new ScrollControl(_upgradeBed.Page) {Dock = Pos.Fill};
            _upgradePillScroll = new ScrollControl(_upgradePill.Page) {Dock = Pos.Fill};
            _upgradeDrinkScroll = new ScrollControl(_upgradeEnergy.Page) {Dock = Pos.Fill};

            _upgradeBedScroll.EnableScroll(false, true);
            _upgradeBedScroll.AutoHideBars = true;

            _upgradePillScroll.EnableScroll(false, true);
            _upgradePillScroll.AutoHideBars = true;

            _upgradeDrinkScroll.EnableScroll(false, true);
            _upgradeDrinkScroll.AutoHideBars = true;

            foreach (Item item in ((Main) Game).GameItems)
            {
                if (item == null) continue;
                if (!item.Buyable) continue;
                if (item.Type == Item.ItemType.Bed) _bedList.Add(item);
                if (item.Type == Item.ItemType.Pill) _pillList.Add(item);
                if (item.Type == Item.ItemType.Drink) _drinkList.Add(item);
            }

            for (int i = 0; i < _bedList.Count; i++)
            {
                new ItemBox(Game, _bedList[i], _upgradeBedScroll, true).GroupBox.SetPosition(4, i*85);
            }

            for (int i = 0; i < _pillList.Count; i++)
            {
                new ItemBox(Game, _pillList[i], _upgradePillScroll, true).GroupBox.SetPosition(4, i*85);
            }

            for (int i = 0; i < _drinkList.Count; i++)
            {
                new ItemBox(Game, _drinkList[i], _upgradeDrinkScroll, true).GroupBox.SetPosition(4, i*85);
            }
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("Shop_title");
            _upgradeBed.Text = ((Main) Game).ActiveLanguage.Get("Shop_bedTab");
            _upgradePill.Text = ((Main) Game).ActiveLanguage.Get("Shop_pillTab");
            _upgradeEnergy.Text = ((Main) Game).ActiveLanguage.Get("Shop_energyTab");
            PopulateTabs();
        }
    }
}