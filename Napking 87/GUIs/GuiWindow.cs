﻿using Gwen.Control;
using KuubEngine.Core;

namespace Napking_87.GUIs
{
    internal abstract class GuiWindow
    {
        protected KuubGameWindow Game;
        public WindowControl Window { get; internal set; }

        public virtual void Update()
        {
        }

        public abstract void LoadLanguage();

        public virtual void ToggleWindow()
        {
            if (Window.IsHidden)
            {
                Window.Show();
            }
            else
            {
                Window.Close();
            }
        }
    }
}