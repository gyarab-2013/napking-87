﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Gwen;
using KuubEngine.Core;
using Gwen.Control;
using KuubEngine.Managers;

namespace Napking_87.GUIs
{
    internal class LanguageSelector : GuiWindow
    {
        private ListBox languageBox;
        private Button confirmButton;
        private int tagCount;
        public List<string> tagNames = new List<string>();

        public LanguageSelector(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, "Select language");
            Window.SetSize(250, 300);
            Window.Position(Pos.Center);
            Window.DisableResizing();
            Window.IsClosable = false;

            languageBox = new ListBox(Window) { Dock = Pos.Fill };

            try
            {
                Language english =
                    Language.FromXML(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                     "/Resources/Languages/en_US.xml");

                tagCount = english.GetAllWords().Count;

                int i = 0;

                foreach (var tagName in english.GetAllWords())
                {
                    tagNames.Add(tagName.Key);
                    i++;
                }
            }
            catch
            {
                Window.Close();
                Util.MessageBox.ShowMessageBox(Game.GwenCanvas,"Error","You've successfully broken the game. Congratulations. Try a clean install.","Ok.").Clicked += delegate { Game.Exit(); };
            }

            foreach (
                var loadedLanguage in
                    Directory.GetFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                       "/Resources/Languages/"))
            {
                try
                {
                    Language lang = Language.FromXML(loadedLanguage);
                    string[] fileName = loadedLanguage.Split('/');

                    if (!CheckLanguage(lang))
                    {
                        languageBox.AddRow(lang.Name + " - Incomplete", fileName[fileName.Length - 1]);
                    }
                    else
                    {
                        languageBox.AddRow(lang.Name, fileName[fileName.Length - 1]);
                    }
                }
                catch
                {
                    string fileName = loadedLanguage.Split('/')[loadedLanguage.Split('/').Length - 1];
                    string languageFile = fileName.Split('.')[0];
                    languageBox.AddRow(languageFile + " - ERROR", "error|" + fileName);
                }
            }

            confirmButton = new Button(Window);
            confirmButton.Dock = Pos.Bottom;
            confirmButton.Text = "Confirm";
            confirmButton.Clicked += delegate
            {
                if (languageBox.SelectedRow == null) return;
                if (languageBox.SelectedRow.Name.Contains("error"))
                {
                    try
                    {
                        Language lang = Language.FromXML(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Resources/Languages/" + languageBox.SelectedRow.Name.Split('|')[1]);
                    }
                    catch(Exception ex)
                    {
                        Util.MessageBox.ShowMessageBox(Game.GwenCanvas,"Error",ex.Message,"Ok");   
                    }
                    return;
                }
                ((DevMain)game).ActiveLanguage =
                    Language.FromXML(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                     "/Resources/Languages/" + languageBox.SelectedRow.Name);
                Window.Close();
                ((DevMain)Game).LoadGameObjects();
                ((DevMain)Game).ItemLoader.ToggleWindow();
                ((DevMain)Game).SetupState();
            };
        }

        public bool CheckLanguage(Language language)
        {
            Dictionary<string, string> words = language.GetAllWords();

            if (words.Count < tagCount) return false;

            foreach (var word in words)
            {
                if (!tagNames.Contains(word.Key))
                {
                    return false;
                }
            }

            return true;
        }

        public override void LoadLanguage()
        {
        }
    }
}