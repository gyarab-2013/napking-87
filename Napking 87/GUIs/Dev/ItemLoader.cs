﻿using System.IO;
using System.Reflection;
using Gwen;
using KuubEngine.Core;
using Gwen.Control;
using KuubEngine.Managers;

namespace Napking_87.GUIs
{
    internal class ItemLoader : GuiWindow
    {

        private ListBox _itemList;

        public ItemLoader(KuubGameWindow game)
        {
            Game = game;

            Window  = new WindowControl(Game.GwenCanvas, ((DevMain)Game).ActiveLanguage.Get("Dev_itemLoaderTitle"));
            Window.SetSize(200, 300);
            Window.Position(Pos.Center);
            Window.DisableResizing();
            Window.IsClosable = false;
            Window.Close();

            _itemList = new ListBox(Window);
            _itemList.Dock = Pos.Fill;
        }

        public override void LoadLanguage()
        {
            Window.Title = ((DevMain) Game).ActiveLanguage.Get("Dev_itemLoaderTitle");
        }
    }
}