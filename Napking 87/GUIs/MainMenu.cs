﻿using Gwen;
using KuubEngine.Core;
using Gwen.Control;

namespace Napking_87.GUIs
{
    internal class MainMenu : GuiWindow
    {
        private ImagePanel _logo;
        private Button _napButton;
        private Button _moneyButton;
        private Button _upgradeButton;
        private Button _exitButton;
        private Button _optionsButton;
        private Button _helpButton;
        private Button _inventoryButton;
        private Button _storyButton;

        public MainMenu(KuubGameWindow game)
        {
            Game = game;

            Window = new WindowControl(Game.GwenCanvas, ((Main) Game).ActiveLanguage.Get("MainMenu_windowName"));
            Window.SetPosition(500, 250);
            Window.IsClosable = false;
            Window.Dock = Pos.Left | Pos.Bottom;
            Window.SetSize(225, 300);
            Window.DisableResizing();

            _napButton = new Button(Window)
                {
                    Dock = Pos.Top,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_napLairButton")
                };
            _napButton.Clicked +=
                delegate { ((States.MainState) ((Main) Game).MainStateInstance).NapGui.ToggleWindow(); };

            _moneyButton = new Button(Window)
                {
                    Dock = Pos.Top,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_bankButton")
                };
            _moneyButton.Clicked +=
                delegate { ((States.MainState) ((Main) Game).MainStateInstance).MoneyGui.ToggleWindow(); };

            _upgradeButton = new Button(Window)
                {
                    Dock = Pos.Top,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_shopButton")
                };
            _upgradeButton.Clicked +=
                delegate { ((States.MainState) ((Main) Game).MainStateInstance).UpgradeGui.ToggleWindow(); };


            _exitButton = new Button(Window)
                {
                    Dock = Pos.Bottom,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_exitButton")
                };
            _exitButton.Clicked +=
                delegate
                    {
                        Util.MessageBox.ShowYesNoMessageBox(Game.GwenCanvas,
                                                            ((Main) Game).ActiveLanguage.Get("MainMenu_exitTitle"),
                                                            ((Main) Game).ActiveLanguage.Get("MainMenu_exitText1"),
                                                            ((Main) Game).ActiveLanguage.Get("MainMenu_exitText2"),
                                                            ((Main) Game).ActiveLanguage.Get("General_yes"),
                                                            ((Main) Game).ActiveLanguage.Get("General_no"),
                                                            ((States.MainState) ((Main) Game).MainStateInstance).Exit,
                                                            null);
                    };

            _optionsButton = new Button(Window)
                {
                    Dock = Pos.Bottom,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_optionsButton")
                };
            _optionsButton.Clicked +=
                delegate { ((States.MainState) ((Main) Game).MainStateInstance).OptionsGui.ToggleWindow(); };

            _helpButton = new Button(Window)
                {
                    Dock = Pos.Bottom,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_helpButton")
                };
            _helpButton.Clicked +=
                delegate { ((States.MainState) ((Main) Game).MainStateInstance).HelpGui.ToggleWindow(); };

            _inventoryButton = new Button(Window)
                {
                    Dock = Pos.Top,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_inventoryButton")
                };
            _inventoryButton.Clicked +=
                delegate { ((States.MainState) ((Main) Game).MainStateInstance).InventoryGui.ToggleWindow(); };

            _storyButton = new Button(Window)
                {
                    Dock = Pos.Top,
                    Text = ((Main) Game).ActiveLanguage.Get("MainMenu_storyButton")
                };
            _storyButton.Clicked +=
                delegate { ((States.MainState) ((Main) Game).MainStateInstance).StoryGui.ToggleWindow(); };

            _logo = new ImagePanel(Game.GwenCanvas) {ImageName = "Resources/logo.png", Width = 500, Height = 102};
        }

        public override void LoadLanguage()
        {
            Window.Title = ((Main) Game).ActiveLanguage.Get("MainMenu_windowName");
            _napButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_napLairButton");
            _moneyButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_bankButton");
            _upgradeButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_shopButton");
            _exitButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_exitButton");
            _optionsButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_optionsButton");
            _inventoryButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_inventoryButton");
            _helpButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_helpButton");
            _storyButton.Text = ((Main) Game).ActiveLanguage.Get("MainMenu_storyButton");
        }
    }
}