﻿using System.Diagnostics;
using KuubEngine.Core;

namespace Napking_87
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                EngineManager.Start<Main>(args, !Debugger.IsAttached);
            }
            else if (args[0] == "-dev")
            {
                EngineManager.Start<DevMain>(args, !Debugger.IsAttached);
            }
            else
            {
                EngineManager.Start<Main>(args, !Debugger.IsAttached);
            }
        }
    }
}