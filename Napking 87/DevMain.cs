﻿using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using Gwen;
using Gwen.Control;
using KuubEngine.Core;
using KuubEngine.Managers;
using Napking_87.States;
using Napking_87.GUIs;

namespace Napking_87
{
    class DevMain : KuubGameWindow
    {
        public DevState DevStateInstance;
        public Language ActiveLanguage;

        private LanguageSelector languageSelector;
        public ItemLoader ItemLoader;
            
        protected override void Initialize()
        {
            new AssemblyName(Assembly.GetCallingAssembly().FullName).Name = "Napking 87 (DevMode)";
            if (!Debugger.IsAttached) Statistics.Visible = false;
            ClearColor = Color.FromArgb(255, 150, 170, 170);
            Title = "Napking 87 (DevMode)";

            Icon = new Icon("icon.ico");

            languageSelector = new LanguageSelector(this);
        }
        public void LoadGameObjects()
        {
            ItemLoader = new ItemLoader(this);
        }

        public void SetupState()
        {
            DevStateInstance = new DevState(this);
            StateManager.Push(DevStateInstance);
            StateManager.Switch(DevStateInstance);
        }
    }
}
