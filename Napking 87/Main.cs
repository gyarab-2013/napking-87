﻿using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using KuubEngine.Managers;
using System.Collections.Generic;
using KuubEngine.Core;
using Napking_87.States;
using Color = System.Drawing.Color;

namespace Napking_87
{
    public class Main : KuubGameWindow
    {
        public readonly List<Item> GameItems = new List<Item>();
        public readonly List<Quest> GameQuests = new List<Quest>();
        internal MainState MainStateInstance;
        public readonly Dictionary<string, Language> LoadedLanguages = new Dictionary<string, Language>();
        public Language ActiveLanguage;
        private LuaHelper LuaHelper;

        protected override void Initialize()
        {
            new AssemblyName(Assembly.GetCallingAssembly().FullName).Name = "Napking 87";
            if (!Debugger.IsAttached) Statistics.Visible = false;
            ClearColor = Color.FromArgb(255, 150, 170, 170);
            Title = "Napking 87";

            Icon = new Icon("icon.ico");

            Player.Load(this);

            LuaHelper = new LuaHelper();
            LoadLua();

            foreach (
                var loadedLanguage in
                    Directory.GetFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                       "/Resources/Languages/"))
            {
                try
                {
                    Language lang = Language.FromXML(loadedLanguage);
                    string[] fileName = loadedLanguage.Split('/');
                    LoadedLanguages.Add(fileName[fileName.Length - 1].Split('.')[0], lang);
                }
                catch{}
            }

            ActiveLanguage = LoadedLanguages[Player.SavedLanguage];

            for (int i = 0; i < 4096; i++)
            {
                GameItems.Add(null);
                GameQuests.Add(null);
            }

            foreach (
                var loadedItem in
                    Directory.GetFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                       "/Resources/Items"))
            {
                Item item = Item.LoadFromXml(loadedItem, ActiveLanguage);

                if (GameItems[item.ID] != null)
                {
                    throw new IDConflictException("ID Conflict! There is already item " + GameItems[item.ID].Name +
                                               " with ID " + item.ID + "!");
                }
                GameItems[item.ID] = item;
            }

            foreach (
                var loadedQuest in
                    Directory.GetFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                       "/Resources/Quests/Xml"))
            {
                Quest quest = Quest.LoadFromXml(loadedQuest, ActiveLanguage);

                if (GameQuests[quest.ID] != null)
                {
                    throw new IDConflictException("ID Conflict! There is already quest " + GameItems[quest.ID].Name +
                                               " with ID " + quest.ID + "!");
                }
                GameQuests[quest.ID] = quest;
            }

            MainStateInstance = new MainState(this);

            Player.MainInstance = MainStateInstance;

            StateManager.Push(MainStateInstance);

            StateManager.Switch(MainStateInstance);

            Player.Load(this);

            MainStateInstance.StoryGui.LoadQuest(Player.Quest, true);

            MainStateInstance.MoneyGui.UpdateBankStatus();
        }

        private void LoadLua()
        {
            Lua.Interface["player"] = LuaHelper;
        }

        protected override void UnloadContent()
        {
            MainStateInstance.Unload();
            base.UnloadContent();
        }
    }
}