﻿using System;
using System.IO;
using System.Xml;
using KuubEngine.Core;
using KuubEngine.Managers;

namespace Napking_87
{
    public class Quest
    {
        private string _questPath;
        public int ID;
        public int ItemIDReward;
        public int MoneyReward;
        public int[] NextQuests;
        public string Name;
        public string Description;
        public string Goal;
        public string Condition;

        public bool CheckCondition(KuubGameWindow game)
        {
            return (bool) game.Lua.DoString("return quest.check()")[0];
        }

        public void Finish(KuubGameWindow game)
        {
            game.Lua.DoString("quest.finish()");
            Player.Money += MoneyReward;
            if (ItemIDReward != -1)
            {
                Player.EquipmentChanged(((Main) game).GameItems[ItemIDReward], true);
            }
            else
            {
                ((States.MainState)((Main)game).MainStateInstance).MoneyGui.UpdateBankStatus();
            }
        }

        public void LoadScript(KuubGameWindow game)
        {
            game.Lua.DoFile("Resources/Quests/Lua/" + Condition);
        }

        public void ReloadInfo(Language activeLanguage)
        {
            var doc = new XmlDocument();

            doc.Load(_questPath);


            XmlNode root = doc.DocumentElement;

            foreach (XmlNode node in root.ChildNodes)
            {

                if (node.Name == "name")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var element = (XmlElement)subNode;
                        if (element.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                            Name = element.InnerText;
                    }
                }

                if (node.Name == "description")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var element = (XmlElement)subNode;
                        if (element.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                            Description = element.InnerText;
                    }
                }

                if (node.Name == "goal")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var element = (XmlElement)subNode;
                        if (element.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                            Goal = element.InnerText;
                    }
                }
            }
        }

        public static Quest LoadFromXml(string path, Language activeLanguage)
        {
            Quest quest = new Quest();

            var doc = new XmlDocument();

            if (File.Exists(path))
            {
                doc.Load(path);
                quest._questPath = path;
                quest.Condition = path.Split('\\')[path.Split('\\').Length - 1].Split('.')[0] + ".lua";
            }
            else
            {
                throw new Exception("Item " + path + " not found!");
            }

            XmlNode root = doc.DocumentElement;

            foreach (XmlNode node in root.ChildNodes)
            {
                var element = (XmlElement)node;
                if (node.Name == "id")
                {
                    quest.ID = int.Parse(element.InnerText);
                }

                else if (node.Name == "rewardID")
                {
                    quest.ItemIDReward = int.Parse(element.InnerText);
                }

                else if (node.Name == "rewardMoney")
                {
                    quest.MoneyReward = int.Parse(element.InnerText);
                }

                else if (node.Name == "nextQuests")
                {
                    quest.NextQuests = new int[node.ChildNodes.Count];
                    for (int i = 0; i < node.ChildNodes.Count; i++)
                    {
                        var subElement = (XmlElement)node.ChildNodes[i];
                        quest.NextQuests[i] = int.Parse(subElement.InnerText);
                    }
                }

                else if (node.Name == "name")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var subElement = (XmlElement)subNode;
                        if (subElement.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                            quest.Name = subElement.InnerText;
                    }
                }

                else if (node.Name == "description")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var subElement = (XmlElement)subNode;
                        if (subElement.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                            quest.Description = subElement.InnerText;
                    }
                }

                else if (node.Name == "goal")
                {
                    foreach (var subNode in node.ChildNodes)
                    {
                        var subElement = (XmlElement)subNode;
                        if (subElement.GetAttribute("lang") == activeLanguage.Get("General_langCode"))
                            quest.Goal = subElement.InnerText;
                    }
                }
            }

            return quest;
        }
    }
}