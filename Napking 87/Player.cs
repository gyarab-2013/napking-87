﻿using System.Globalization;
using System.IO;
using System.Reflection;
using System;
using System.Xml;
using System.Collections.Generic;

namespace Napking_87
{
    internal static class Player
    {
        public static States.MainState MainInstance;
        public static readonly TimeSpan ConstMinNapTime = new TimeSpan(0, 0, 1, 0);
        public static readonly TimeSpan ConstMaxNapTime = new TimeSpan(0, 1, 0, 0);
        public static TimeSpan MinNapTime = new TimeSpan(0, 0, 1, 0);
        public static TimeSpan MaxNapTime = new TimeSpan(0, 1, 0, 0);
        public static TimeSpan TotalNapTime;
        public static float Money;
        public static float MoneyPerMinute = 1;
        public static int BaseIncome = 1;
        public static string SavedLanguage;
        public static int First;
        public static List<Item> Items = new List<Item>();
        public static Quest Quest;
        public static int QuestVar;

        public static void EquipmentChanged(Item item, bool added)
        {
            float totalNapModifier = 1;
            float totalIncomeModifier = 1;

            if (added)
            {
                Items[(int) item.Type] = item;
            }
            else
            {
                if (!Items.Contains(item)) return;
                Items[(int) item.Type] = null;
            }

            foreach (Item playerItem in Items)
            {
                if (playerItem == null) continue;
                totalNapModifier += playerItem.NapLengthModifier - 1;
                totalIncomeModifier += playerItem.IncomeModifier - 1;
            }

            if (totalNapModifier > 0)
            {
                MaxNapTime = new TimeSpan(0, 0, (int) (ConstMaxNapTime.TotalSeconds*totalNapModifier));
            }
            else
            {
                MinNapTime = new TimeSpan(0, 0, (int) (ConstMinNapTime.TotalSeconds*Math.Abs(totalNapModifier)));
            }

            MoneyPerMinute = BaseIncome*totalIncomeModifier;

            MainInstance.NapGui.SliderChange(null, EventArgs.Empty);
            MainInstance.InventoryGui.UpdateItems();
            MainInstance.MoneyGui.UpdateBankStatus();
        }

        public static void Load(KuubEngine.Core.KuubGameWindow game)
        {
            bool newGame = false;

            Items.Clear();
            Items.Add(null);
            Items.Add(null);
            Items.Add(null);

            var doc = new XmlDocument();

            if (File.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/player.xml"))
            {
                doc.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/player.xml");
            }
            else
            {
                doc.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/default.xml");
                newGame = true;
            }

            XmlNode root = doc.DocumentElement;

            foreach (XmlNode node in root.ChildNodes)
            {
                if (node.Name == "money")
                {
                    var element = (XmlElement) node;
                    Money = float.Parse(element.InnerText);
                    continue;
                }
                if (node.Name == "income")
                {
                    var element = (XmlElement) node;
                    BaseIncome = int.Parse(element.InnerText);
                    continue;
                }
                if (node.Name == "totalNapTime")
                {
                    var element = (XmlElement) node;
                    TotalNapTime = new TimeSpan(0, 0, (int) double.Parse(element.InnerText));
                    continue;
                }
                if (node.Name == "language")
                {
                    var element = (XmlElement) node;
                    SavedLanguage = element.InnerText;
                    continue;
                }
                if (node.Name == "first")
                {
                    var element = (XmlElement) node;
                    First = int.Parse(element.InnerText);
                    continue;
                }
                if (node.Name == "upgrades")
                {
                    if (((Main) game).GameItems.Count == 0) continue;
                    var element = (XmlElement) node;
                    foreach (XmlNode subNode in element.ChildNodes)
                    {
                        Item item = ((Main) game).GameItems[int.Parse(subNode.Attributes["id"].InnerText)].Clone();
                        if (item == null) throw new InvalidDataException("There is no item with ID " + item.ID + "!");
                        item.UsesLeft = int.Parse(subNode.ChildNodes[0].InnerText);
                        item.TimeLeft = float.Parse(subNode.ChildNodes[1].InnerText.Replace('.', ','));
                        EquipmentChanged(item, true);
                    }
                    continue;
                }
                if (node.Name == "quest")
                {
                    if (((Main)game).GameQuests.Count == 0) continue;
                    var element = (XmlElement) node;
                    Quest = ((Main)game).GameQuests[int.Parse(element.InnerText)];
                    if (newGame)
                    {
                        Quest.LoadScript(game);
                        game.Lua.DoString("quest.init()");
                    }
                }
                if (node.Name == "questVar")
                {
                    var element = (XmlElement) node;
                    QuestVar = int.Parse(element.InnerText);
                }
            }
        }

        public static void Save()
        {
            var doc = new XmlDocument();
            var declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);
            XmlElement root = doc.CreateElement("player");

            XmlElement money = doc.CreateElement("money");
            money.InnerText = Money.ToString();

            XmlElement income = doc.CreateElement("income");
            income.InnerText = BaseIncome.ToString();

            XmlElement totalNapTime = doc.CreateElement("totalNapTime");
            totalNapTime.InnerText = TotalNapTime.TotalSeconds.ToString();

            XmlElement savedLanguage = doc.CreateElement("language");
            savedLanguage.InnerText = SavedLanguage;

            XmlElement first = doc.CreateElement("first");
            first.InnerText = First.ToString(CultureInfo.InvariantCulture);

            XmlElement quest = doc.CreateElement("quest");
            quest.InnerText = Quest.ID.ToString();

            XmlElement questVar = doc.CreateElement("questVar");
            questVar.InnerText = QuestVar.ToString();

            XmlElement upgrades = doc.CreateElement("upgrades");

            foreach (Item item in Items)
            {
                if (item == null) continue;

                XmlElement upgrade = doc.CreateElement("upgrade");
                upgrade.SetAttribute("id", item.ID.ToString(CultureInfo.InvariantCulture));

                XmlElement uses = doc.CreateElement("uses");
                uses.InnerText = item.UsesLeft.ToString(CultureInfo.InvariantCulture);
                upgrade.AppendChild(uses);

                XmlElement timeUses = doc.CreateElement("timeUses");
                timeUses.InnerText = item.TimeLeft.ToString(CultureInfo.InvariantCulture);
                upgrade.AppendChild(timeUses);

                upgrades.AppendChild(upgrade);
            }

            root.AppendChild(money);
            root.AppendChild(income);
            root.AppendChild(totalNapTime);
            root.AppendChild(savedLanguage);
            root.AppendChild(first);
            root.AppendChild(quest);
            root.AppendChild(upgrades);
            root.AppendChild(questVar);

            doc.AppendChild(root);
            doc.Save(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/player.xml");
        }
    }
}